﻿
using System.Collections.Generic;
using System.Linq;
using HYVE.Jigsaw.DataProviders.BLL.Models;
using HYVE.Jigsaw.DataProviders.DAL.Relational;
using HYVE.Jigsaw.DataProviders.DAL.Solr;
using HYVE.Jigsaw.DataProviders.Models;
using HYVE.Jigsaw.Models;

namespace HYVE.Jigsaw.BLL
{
    public class ExplorerService : IExplorerService
    {
        private readonly ISolrWrapper<SolrDoc> _solrWrapper;
        private readonly IImageWrapper<SolrDoc> _imageWrapper;
        private readonly IRelationalDbWrapper<DbMention> _dbWrapper;
        public ExplorerService(ISolrWrapper<SolrDoc> solrWrapper, IRelationalDbWrapper<DbMention> dbWrapper, IImageWrapper<SolrDoc> imageWrapper)
        {
            _solrWrapper = solrWrapper;
            _dbWrapper = dbWrapper;
            _imageWrapper = imageWrapper;

        }

        public ExplorerResult<Mention> Explore(PostsSearchParams searchParams)
        {
            var solrMentions = _solrWrapper.Query(searchParams);

            var idsList = new List<string>();
            solrMentions.Result.ForEach(a => { if (!string.IsNullOrEmpty(a.Id)) idsList.Add(a.Id); });

            var mentionsDictionary = _dbWrapper
                 .Query(solrMentions.Result.Select(m => m.Id).ToList())
                 .Result
                 .Aggregate(new Dictionary<string, DbMention>(), (p, c) =>
                 {
                     p[c.PostId] = c;
                     return p;
                 });

            var mentions = MapMentions(solrMentions, mentionsDictionary);

            return new ExplorerResult<Mention>
            {
                Result = mentions,
                TotalCount = solrMentions.TotalCount ?? 0
            };
        }

        public ExplorerResult<Mention> ExploreImages(ImageSearchParams searchParams)
        {
            var solrMentions = _imageWrapper.Query(searchParams);

            var idsList = new List<string>();
            solrMentions.Result.ForEach(a => { if (!string.IsNullOrEmpty(a.Id)) idsList.Add(a.Id); });

            var mentionsDictionary = _dbWrapper
                 .Query(solrMentions.Result.Select(m => m.Id).ToList())
                 .Result
                 .Aggregate(new Dictionary<string, DbMention>(), (p, c) =>
                 {
                     p[c.PostId] = c;
                     return p;
                 });

            var mentions = MapMentions(solrMentions, mentionsDictionary);

            return new ExplorerResult<Mention>
            {
                Result = mentions,
                TotalCount = solrMentions.TotalCount ?? 0
            };
        }

        private static List<Mention> MapMentions(DataProviders.DAL.Models.WrapperResult<SolrDoc> solrMentions, IDictionary<string, DbMention> mentionsDictionary)
        {
            return solrMentions.Result.Select(m =>
            {
                try
                {
                    var mention = mentionsDictionary[m.Id];
                    return new Mention
                    {
                        AuthorId = mention.AuthorId,
                        Cases = mention.Cases,
                        Clans = mention.Clans,
                        Comments = mention.Comments,
                        DataSource = mention.DataSource,
                        Date = mention.Date,
                        FollowersCount = mention.FollowersCount,
                        Language = mention.Language,
                        Likes = mention.Likes,
                        Name = mention.Name,
                        PostId = mention.PostId,
                        ProfileImage = mention.ProfileImage,
                        Rules = mention.Rules,
                        ScreenName = mention.ScreenName,
                        SentimentId = mention.SentimentId,
                        Shares = mention.Shares,
                        Text = mention.Text,
                        Topic = mention.Topic,
                        UserId = mention.UserId,
                        Media_Url = mention.Media_Url
                    };
                }
                catch {
                    return new Mention();
                } 
                
            }).ToList();
        }
    }
}
