﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HYVE.Jigsaw.DataProviders.BLL.Models
{
    public class ExplorerResult<T>
    {
        public List<T> Result { get; set; }
        public int TotalCount { get; set; }
    }
}
