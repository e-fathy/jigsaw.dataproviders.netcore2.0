﻿using System;
using System.Collections.Generic;
using System.Text;
using HYVE.Jigsaw.DataProviders.BLL.Models;
using HYVE.Jigsaw.DataProviders.Models;
using HYVE.Jigsaw.Models;

namespace HYVE.Jigsaw.BLL
{
    public interface IExplorerService
    {
        ExplorerResult<Mention> Explore(PostsSearchParams searchParams);
        ExplorerResult<Mention> ExploreImages(ImageSearchParams searchParams);

    }
}
