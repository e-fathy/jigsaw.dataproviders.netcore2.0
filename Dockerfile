FROM microsoft/aspnetcore:2.0-jessie

VOLUME ["/opt/data"]

WORKDIR /app
COPY . .
COPY clidriver /app/clidriver
# install libxml2 for DB2 driver
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        libxml2
ENV LD_LIBRARY_PATH="/app/clidriver/lib/"
Env PATH=$PATH:"/app/clidriver/bin:/app/clidriver/lib"
ENTRYPOINT ["dotnet", "HYVE.Jigsaw.DataProviderss.dll"]