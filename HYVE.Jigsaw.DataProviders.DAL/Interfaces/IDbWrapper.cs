﻿
using System.Collections.Generic;
using HYVE.Jigsaw.DataProviders.DAL.Models;

namespace HYVE.Jigsaw.DataProviders.DAL.Interfaces
{
    public interface IDbWrapper<in TParams, TResult>
    {

        WrapperResult<TResult> Query(TParams searchParams);

    }
}
