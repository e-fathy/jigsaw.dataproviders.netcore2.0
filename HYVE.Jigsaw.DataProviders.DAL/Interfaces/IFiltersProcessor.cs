﻿using System.Collections.Generic;

namespace HYVE.Jigsaw.DataProviders.DAL.Interfaces
{
    public interface IFiltersProcessor<in TSource, out TTarget>
    {
        TTarget Process(TSource searchParams);
    }
}
