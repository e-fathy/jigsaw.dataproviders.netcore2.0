﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace HYVE.Jigsaw.DataProviders.DAL.Solr
{
    public class Result
    {
        [JsonProperty("mention_id")]
        public string MentionId { get; set; }
        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }

    }
    public class SolrDocs
    {
        public List<Result> Docs { get; set; }
        public string Count { get; set; }

        public SolrDocs(string json)
        {
            var jObject = JObject.Parse(json);
            var set = jObject["result-set"];
            Docs = set["docs"].ToObject<List<Result>>();
            Docs.RemoveAt(Docs.Count - 1);
            Count = (string)jObject["count"];
        }
    }


    public class SolrDoc: Models.IMappable<string>
    {
        public string Id { get; set; }
        public void From(string obj)
        {
            Id = obj;
        }
    }


}
