﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using HYVE.Jigsaw.DataProviders.DAL.Models;
using HYVE.Jigsaw.DataProviders.Models;
using HYVE.Jigsaw.Models;
using Microsoft.Extensions.Options;
using RestSharp;

namespace HYVE.Jigsaw.DataProviders.DAL.Solr
{
    public class ImageWrapper<T> : IImageWrapper<T> where T : Models.IMappable<string>
    {
        private readonly AppSettings _appSettings;

        public ImageWrapper(IOptions<AppSettings> settings)
        {
            _appSettings = settings.Value;
        }
        public WrapperResult<T> Query(ImageSearchParams searchParams)
        {

            IRestClient client = new RestClient(_appSettings.SolrImageSearchApi);

            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(searchParams);


            var response = client.Execute(request);
            if (HttpStatusCode.OK != response.StatusCode)
                throw new System.Exception("Error While Calling The Server :" + response.Content);

            if (string.IsNullOrEmpty(response.Content))
                throw new System.Exception("Can't Reach The Server");

            var docs = new SolrDocs(response.Content);
            var res = docs.Docs.Select(doc =>
            {
                var mapped = Activator.CreateInstance<T>();
                mapped.From(doc.MentionId);
                return mapped;
            }).ToList();
            return new WrapperResult<T>
            {
                Result = res,
                TotalCount = int.Parse(docs.Count)
            };
        }
    }
}
