﻿using HYVE.Jigsaw.DataProviders.DAL.Interfaces;
using HYVE.Jigsaw.Models;

namespace HYVE.Jigsaw.DataProviders.DAL.Solr
{
    public interface ISolrWrapper<T>: IDbWrapper<PostsSearchParams, T>
    {

    }
}
