﻿using System.Collections.Generic;
using System.Linq;
using HYVE.Jigsaw.DataProviders.DAL.Interfaces;
using HYVE.Jigsaw.Models;

namespace HYVE.Jigsaw.DataProviders.DAL.Solr
{
    public interface ISolrFiltersProcessor : IFiltersProcessor<PostsSearchParams, SolrSearchParams>
    {
    }

    public class SolrFiltersProcessor: ISolrFiltersProcessor
    {
        public SolrSearchParams Process(PostsSearchParams searchParams)
        {
            var sp = new SolrSearchParams
            {
                
                FilterPageInputOut = new SolrFilterDto
                {
                    Clans = searchParams.Clans,
                    Cases = searchParams.Cases,
                    Categories = searchParams.Categories,
                    DataSources = searchParams.DataSources,
                    ExcludedRules = searchParams.ExcludedRules,
                    EndDate = searchParams.EndDate,
                    EndDateUser = searchParams.EndDateUser,
                    ExcludedStreams = searchParams.ExcludedStreams,
                    IncludedRules = searchParams.IncludedRules,
                    IncludedStreams = searchParams.IncludedStreams,
                    Sentiment = searchParams.Sentiment,
                    StartDate = searchParams.StartDate,
                    StartDateUser = searchParams.StartDateUser,
                    Topics = searchParams.Topics
                },

                PagedAndSortedInputDto = new PagedAndSortedSolrDto
                {
                    MaxResultCount = searchParams.Take,
                    SkipCount = searchParams.Skip,
                    Sorting = searchParams.SortBy.Select(s =>
                    {
                        var sort = s.Split(' ');
                        return new KeyValuePair<string, string>(sort[0], sort.Length > 1 ? sort[1] : string.Empty);
                    }).ToList()
                },

                Content = searchParams.Content,


            };

            return sp;
        }
    }
}
