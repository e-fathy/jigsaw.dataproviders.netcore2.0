﻿using Newtonsoft.Json;

namespace HYVE.Jigsaw.DataProviders.DAL.Solr
{
    public class SolrSearchParams
    {
        public PagedAndSortedSolrDto PagedAndSortedInputDto { get; set; }
        public SolrFilterDto FilterPageInputOut { get; set;}
        [JsonProperty("content", Order =1)]
        public string Content { get; set; }

    }
}
