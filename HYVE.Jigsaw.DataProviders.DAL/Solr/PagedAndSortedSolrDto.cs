﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace HYVE.Jigsaw.DataProviders.DAL.Solr
{
   public class PagedAndSortedSolrDto
    {
        public PagedAndSortedSolrDto()
        {
            Sorting = new List<KeyValuePair<string, string>>();
        }
        [JsonProperty("sorting")]
        public List<KeyValuePair<string, string>> Sorting { get; set; }

        [JsonProperty("maxResultCount")]
        public int MaxResultCount { get; set; }

        [JsonProperty("skipCount")]
        public int SkipCount { get; set; }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
