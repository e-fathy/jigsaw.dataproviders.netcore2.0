﻿using HYVE.Jigsaw.DataProviders.DAL.Interfaces;
using HYVE.Jigsaw.DataProviders.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace HYVE.Jigsaw.DataProviders.DAL.Solr
{
    public interface IImageWrapper<T> :IDbWrapper<ImageSearchParams,T>
    {
    }
}
