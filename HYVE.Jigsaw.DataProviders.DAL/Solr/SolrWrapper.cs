﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using HYVE.Jigsaw.DataProviders.DAL.Models;
using HYVE.Jigsaw.DataProviders.Models;
using HYVE.Jigsaw.Models;
using Microsoft.Extensions.Options;
using RestSharp;

namespace HYVE.Jigsaw.DataProviders.DAL.Solr
{
    public class SolrWrapper<T> : ISolrWrapper<T> where T : Models.IMappable<string>
    {
        private readonly ISolrFiltersProcessor _solrFiltersProcessor;
        private readonly AppSettings _appSettings;

        public SolrWrapper(ISolrFiltersProcessor solrFiltersProcessor, IOptions<AppSettings> settings)
        {
            _appSettings = settings.Value;
            _solrFiltersProcessor = solrFiltersProcessor;
        }

        public WrapperResult<T> Query(PostsSearchParams searchParams)
        {
            var body = _solrFiltersProcessor.Process(searchParams);

            IRestClient client = new RestClient(_appSettings.SolrStreamingExpressionApi);

            var request = new RestRequest(Method.POST);
            request.AddHeader("Content-Type", "application/json");
            request.AddJsonBody(body);


            var response = client.Execute(request);
            if (HttpStatusCode.OK != response.StatusCode)
                throw new System.Exception("Error While Calling The Server :" + response.Content);

            if (string.IsNullOrEmpty(response.Content))
                throw new System.Exception("Can't Reach The Server");

            var docs = new SolrDocs(response.Content);
            var res = docs.Docs.Select(doc =>
            {
                var mapped = Activator.CreateInstance<T>();
                mapped.From(doc.MentionId);
                return mapped;
            }).ToList();
            return new WrapperResult<T>
            {
                Result = res,
                TotalCount = int.Parse(docs.Count)
            };

        }
    }
}
