﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace HYVE.Jigsaw.DataProviders.DAL.Solr
{
    public class SolrFilterDto
    {
        [JsonProperty("startDate")]
        public string StartDate { get; set; }
        [JsonProperty("endDate")]
        public string EndDate { get; set; }
        [JsonProperty("startDateUser")]
        public string StartDateUser { get; set; }
        [JsonProperty("endDateUser")]
        public string EndDateUser { get; set; }
        [JsonProperty("includedStreams")]
        public List<int> IncludedStreams { get; set; }
        [JsonProperty("excludedStreams")]
        public List<int> ExcludedStreams { get; set; }
        [JsonProperty("dataSources")]
        public List<string> DataSources { get; set; }
        [JsonProperty("includedRules")]
        public List<string> IncludedRules { get; set; }
        [JsonProperty("excludedRules")]
        public List<string> ExcludedRules { get; set; }
        [JsonProperty("categories")]
        public List<int> Categories { get; set; }
        [JsonProperty("topics")]
        public List<int> Topics { get; set; }
        [JsonProperty("sentiment")]
        public List<int> Sentiment { get; set; }
        [JsonProperty("clans")]
        public List<int> Clans { get; set; }
        [JsonProperty("cases")]
        public List<int> Cases { get; set; }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
