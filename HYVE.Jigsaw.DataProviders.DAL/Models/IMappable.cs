﻿namespace HYVE.Jigsaw.DataProviders.DAL.Models
{
    public interface IMappable<in T>
    {
        void From(T obj);
    }
}
