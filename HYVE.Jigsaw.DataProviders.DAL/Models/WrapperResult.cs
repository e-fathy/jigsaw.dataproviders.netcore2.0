﻿using System.Collections.Generic;

namespace HYVE.Jigsaw.DataProviders.DAL.Models
{
    public class WrapperResult<T>
    {
        public int? TotalCount { get; set; }
        public List<T> Result { get; set; }
    }
}
