﻿using System.Collections.Generic;
using HYVE.Jigsaw.DataProviders.DAL.Interfaces;

namespace HYVE.Jigsaw.DataProviders.DAL.Relational
{
    public interface IRelationalDbWrapper<TResult> : IDbWrapper<List<string>, TResult>
    {
    }
}
