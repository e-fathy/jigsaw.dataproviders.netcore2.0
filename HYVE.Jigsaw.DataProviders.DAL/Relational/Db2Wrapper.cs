﻿using System;
using System.Collections.Generic;
using HYVE.Jigsaw.DataProviders.DAL.Models;
using HYVE.Jigsaw.Models;
using IBM.Data.DB2.Core;
using Microsoft.Extensions.Options;

namespace HYVE.Jigsaw.DataProviders.DAL.Relational
{
    public class Db2Wrapper<T> : IRelationalDbWrapper<T> where T : IMappable<DB2DataReader>
    {
        private readonly IRelationalDbFilterProcessor _relationalDbFilterProcessor;
        private readonly AppSettings _appSettings;
        private DB2Connection _db2Connection;

        public Db2Wrapper(IRelationalDbFilterProcessor relationalDbFilterProcessor, IOptions<AppSettings> appSettings)
        {
            _relationalDbFilterProcessor = relationalDbFilterProcessor;
            _appSettings = appSettings.Value;
        }
        public void Connect(string connectionString)
        {
            _db2Connection = new DB2Connection(connectionString);
            try
            {
                _db2Connection.Open();
            }
            catch (DB2Exception ex)
            {
                _db2Connection.Close();
            }
        }
        public WrapperResult<T> Query(List<string> searchParams)
        {
            if (_db2Connection == null || !_db2Connection.IsOpen)
                Connect(_appSettings.Db2ConnectionString);
            var sql = _relationalDbFilterProcessor.Process(searchParams);
            var cmd = new DB2Command(sql, _db2Connection);
            var reader = cmd.ExecuteReader();
            var res = new List<T>();
            while (reader.Read())
            {
                var mapped = Activator.CreateInstance<T>();
                mapped.From(reader);
                res.Add(mapped);
            };
            return new WrapperResult<T>
            {
                Result = res,
                TotalCount = null
            };
        }
    }
}