﻿using IBM.Data.DB2.Core;

namespace HYVE.Jigsaw.DataProviders.DAL.Relational
{
    public class DbMention: Models.IMappable<DB2DataReader>
    {
        public string PostId { get; set; }
        public string AuthorId { get; set; }
        public string Topic { get; set; }
        public string SentimentId { get; set; }
        public string UserId { get; set; }
        public string ScreenName { get; set; }
        public string Name { get; set; }
        public string FollowersCount { get; set; }
        public string ProfileImage { get; set; }
        public string Date { get; set; }
        public string Text { get; set; }
        public string Shares { get; set; }
        public string Likes { get; set; }
        public string Comments { get; set; }
        public string Language { get; set; }
        public string DataSource { get; set; }
        public string Clans { get; set; }
        public string Cases { get; set; }
        public string Rules { get; set; }
        public string Media_Url { get; set; }

        public void From(DB2DataReader obj)
        {
            AuthorId = obj["AUTHORID"].ToString();
            Cases = obj["CASES"].ToString();
            Clans = obj["CLANS"].ToString();
            Comments = obj["COMMENTS"].ToString();
            DataSource = obj["DATASOURCE"].ToString();
            Date = obj["DATE"].ToString();
            FollowersCount = obj["FOLLOWERSCOUNT"].ToString();
            Language = obj["LANGUAGE"].ToString();
            Likes = obj["LIKES"].ToString();
            Name = obj["NAME"].ToString();
            PostId = obj["POSTID"].ToString();
            ProfileImage = obj["PROFILEIMAGE"].ToString();
            Rules = obj["RULES"].ToString();
            ScreenName = obj["SCREENNAME"].ToString();
            SentimentId = obj["SENTIMENTID"].ToString();
            Shares = obj["SHARES"].ToString();
            Text = obj["TEXT"].ToString();
            Topic = obj["TOPIC"].ToString();
            UserId = obj["USERID"].ToString();
            Media_Url = obj["Media_Url"].ToString();
        }
    }
}
