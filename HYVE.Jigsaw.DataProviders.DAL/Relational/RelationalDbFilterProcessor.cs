﻿using HYVE.Jigsaw.DataProviders.DAL.Interfaces;
using HYVE.Jigsaw.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HYVE.Jigsaw.DataProviders.DAL.Relational
{
    public interface IRelationalDbFilterProcessor : IFiltersProcessor<List<string>, string>
    {

    }

    public class RelationalDbFilterProcessor : IRelationalDbFilterProcessor
    {
        public string Process(List<string> ids)
        {
            var queryString = new StringBuilder(); ;
            queryString.Append("SELECT * FROM jigsaw.jigsaw_mentions WHERE  ");
            if (ids.Count == 1)
            {
                queryString.Append("POSTID = '" + ids.First() + "'");
            }
            else
            {
                queryString.Append("POSTID in ('" + string.Join("','", ids) + "')");
            }
            return queryString.ToString();
        }
    }
}
