﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HYVE.Jigsaw.BLL;
using HYVE.Jigsaw.DataProviders.DAL.Relational;
using HYVE.Jigsaw.DataProviders.DAL.Solr;
using HYVE.Jigsaw.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;

namespace HYVE.Jigsaw.DataProviderss
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" });
            });

            services.AddScoped<IRelationalDbWrapper<DbMention>, Db2Wrapper<DbMention>>();
            services.AddScoped<ISolrWrapper<SolrDoc>, SolrWrapper<SolrDoc>>();
            services.AddScoped<IImageWrapper<SolrDoc>, ImageWrapper<SolrDoc>>();
            services.AddScoped<IRelationalDbWrapper<DbMention>, Db2Wrapper<DbMention>>();
            services.AddScoped<IExplorerService, ExplorerService>();
            services.AddScoped<IRelationalDbFilterProcessor, RelationalDbFilterProcessor>();
            services.AddScoped<ISolrFiltersProcessor, SolrFiltersProcessor>();

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");

            });
            app.UseMvc();
        }
    }
}
