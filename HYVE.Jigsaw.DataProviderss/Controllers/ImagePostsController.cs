﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HYVE.Jigsaw.BLL;
using HYVE.Jigsaw.DataProviders.BLL.Models;
using HYVE.Jigsaw.DataProviders.Models;
using HYVE.Jigsaw.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HYVE.Jigsaw.DataProviderss.Controllers
{

    [Route("api/ImagePosts")]
    public class ImagePostsController : Controller
    {
        private readonly IExplorerService _explorerService;
        public ImagePostsController(IExplorerService explorerService)
        {
            _explorerService = explorerService;
        }

        [HttpPost]
        public ExplorerResult<Mention> Get([FromBody]ImageSearchParams searchParams)

        {
            return _explorerService.ExploreImages(searchParams);
        }
    }
}