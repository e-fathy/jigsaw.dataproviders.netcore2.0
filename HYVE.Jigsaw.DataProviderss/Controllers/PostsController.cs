﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PostsSearchParams = HYVE.Jigsaw.Models.PostsSearchParams;
using HYVE.Jigsaw.BLL;
using HYVE.Jigsaw.DataProviders.BLL.Models;
using HYVE.Jigsaw.Models;

namespace HYVE.Jigsaw.DataProviders.API.Controllers
{
    [Route("api/[controller]")]
    
    public class PostsController : Controller
    {
        
        private readonly IExplorerService _explorerService;
        public PostsController(IExplorerService explorerService) {
            _explorerService = explorerService;
        }

        [HttpPost]
        public ExplorerResult<Mention> Get([FromBody]PostsSearchParams searchParams)

        {
            return _explorerService.Explore(searchParams);
        }
    }
}