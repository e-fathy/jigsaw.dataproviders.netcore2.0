﻿
using HYVE.Jigsaw.BLL;
using HYVE.Jigsaw.DataProviders.BLL.Models;
using HYVE.Jigsaw.DataProviders.DAL.Relational;
using HYVE.Jigsaw.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace HYVE.Jigsaw.DataProviders.API.Controllers
{
    [Route("api/[controller]")]
    public class TestConnectionController : Controller
    {
        private readonly IRelationalDbWrapper<DbMention> _relationalDbService;
        private readonly IExplorerService _explorerServices;

        public TestConnectionController(IRelationalDbWrapper<DbMention> relationalDbService, IExplorerService explorerServices)
        {
            _relationalDbService = relationalDbService;
            _explorerServices = explorerServices;
        }

        [HttpGet]
        public ExplorerResult<Mention> Get()
        {           
            return _explorerServices.Explore(new PostsSearchParams
            {
                SortBy = new List<string>{"created_at desc"},
                Take = 100,
                Skip = 1,
                StartDate = "2018-07-16 00:00:00.000",
                EndDate = "2018-10-22 00:00:00.000",
                DataSources = new List<string> { "Facebook" }
            });
        }
    }
       
}