﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HYVE.Jigsaw.Models
{
    public class Mention
    {
        public string PostId { get; set; }
        public string AuthorId { get; set; }
        public string Topic { get; set; }
        public string SentimentId { get; set; }
        public string UserId { get; set; }
        public string ScreenName { get; set; }
        public string Name { get; set; }
        public string FollowersCount { get; set; }
        public string ProfileImage { get; set; }
        public string Date { get; set; }
        public string Text { get; set; }
        public string Shares { get; set; }
        public string Likes { get; set; }
        public string Comments { get; set; }
        public string Language { get; set; }
        public string DataSource { get; set; }
        public string Clans { get; set; }
        public string Cases { get; set; }
        public string Rules { get; set; }
        public string Media_Url { get; set; }
    }
}
