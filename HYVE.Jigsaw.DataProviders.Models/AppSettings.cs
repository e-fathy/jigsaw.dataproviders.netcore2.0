﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HYVE.Jigsaw.Models
{
    public class AppSettings
    {
        public string SolrApiService { get; set; }
        public string SolrExpressionGeneratorApi { get; set; }
        public string Db2ConnectionString { get; set; }     
        public string SolrStreamingExpressionApi { get; set; }
        public string SolrImageSearchApi { get; set; }

        


    }
}
