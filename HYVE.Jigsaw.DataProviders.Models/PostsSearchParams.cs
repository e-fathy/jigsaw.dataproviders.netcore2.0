﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace HYVE.Jigsaw.Models
{

    public class PostsSearchParams
    {
        [JsonProperty("StartDate")]
        public string StartDate { get; set; }
        [JsonProperty("EndDate")]
        public string EndDate { get; set; }
        [JsonProperty("StartDateUser")]
        public string StartDateUser { get; set; }
        [JsonProperty("EndDateUser")]
        public string EndDateUser { get; set; }
        [JsonProperty("Sentiment")]
        public List<int> Sentiment { get; set; }
        [JsonProperty("IncludedStreams")]
        public List<int> IncludedStreams { get; set; }
        [JsonProperty("DataSources")]
        public List<string> DataSources { get; set; }
        [JsonProperty("IncludedRules")]
        public List<string> IncludedRules { get; set; }
        [JsonProperty("ExcludedStreams")]
        public List<int> ExcludedStreams { get; set; }
        [JsonProperty("ExcludedRules")]
        public List<string> ExcludedRules { get; set; }
        [JsonProperty("Categories")]
        public List<int> Categories { get; set; }
        [JsonProperty("Topics")]
        public List<int> Topics { get; set; }
        [JsonProperty("Clans")]
        public List<int> Clans { get; set; }
        [JsonProperty("Cases")]
        public List<int> Cases { get; set; }
        [JsonProperty("SortBy")]
        public List<string> SortBy { get; set; }
        [JsonProperty("Take")]
        public int Take { get; set; }
        [JsonProperty("Skip")]
        public int Skip { get; set; }
        [JsonProperty("Content")]
        public string Content { get; set; }

    }
}
