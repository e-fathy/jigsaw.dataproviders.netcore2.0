﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace HYVE.Jigsaw.DataProviders.Models
{
    public class ImageSearchParams
    {
        [JsonProperty("Hash")]
        public string Hash { get; set; }
        [JsonProperty("Skip")]
        public int Skip{ get; set; }
        [JsonProperty("Take")]
        public int Take{ get; set; }

    }
}
